+++
title = "Over Moonstreet"

+++

Moonstreet is genoemd naar de straatnaam waar wij zijn gevestigd. Maar je zult ons daar niet vaak treffen. We zijn vaak onderweg. Bij een klant of bij een vaste opdracht aan het werk. Leuke dingen aan het doen met code, infrastructuur, cloud en security. En daar schrijven we graag over.

![alt text](http://www.krevh.com/wp-content/uploads/2015/09/lang.png "Zo maar leuk")
