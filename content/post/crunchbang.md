+++
date = "2013-11-10"
title = "Crunchbang"
categories = ["Crunchbang","Linux"]
tags = ["linux"]
+++


I have been using Linux forever. In 2003, I registered at Linux Counter.

![this](https://linuxcounter.net/cert/312740.png).

I have been using tons of Linux distro's. I've started with Caldera, went on to RedHat and used Slackware for a while. After then I sorta stuck to Debian and Ubuntu. And recently I've found [CrunchBang](http://crunchbang.org/).

I think it's quite good.

![crunchbang](/images/crunch.png "Crunch")


### Why CrunchBang is cool #! ###

* lots of keyboard shortcuts
* terminal here in the file explorer
* easy software install fromt OpenBox menu
* feels like home: #! is based on Debian Wheezy

Here's a [review](http://www.everydaylinuxuser.com/2013/03/everyday-linux-user-review-of.html).
I did some minimal tweaks, let's describe them below.


## Conky ##
To use Conky to display keyboard shortcuts is brilliant for those with memory leaks.
I've added some exta shortcuts for VIM, NERDTree and Terminator.
[Here](https://gist.github.com/jacqinthebox/7612954) is a link to my conkyrc.

![crunch-empty](/images/crunch-empty.png "Empty")


## Terminal emulator ###
The default terminal is [Terminator](https://launchpad.net/terminator/).
Here are the most basic keybindings, copied from the [CrunchBang site](http://crunchbanglinux.org/wiki/terminator):

* Ctrl-Shift-o: Split the screen horizontally
* Ctrl-Shift-e: Split the screen vertically
* Ctrl-Shift-t: Open a new tab
* Ctrl-Shift-w: Close the current terminal
* Ctrl-Shift-q: Quit Terminator

Navigation:

* Ctrl-Shift-n / Ctrl-Tab: Next terminal within tab
* Ctrl-Shift-p / Ctrl-Shift-Tab: Previous terminal within tab
* Ctrl-Pageup: Move to next tab
* Ctrl-Pagedown: Move to previous tab
* Shift-Pageup: Scroll back through the current terminal
* Shift-Pagedown: Scroll forward through the current terminal

I decided to change the colorscheme to Solarized from Ethan Schoonover.

<img src="http://ethanschoonover.com/solarized/img/solarized-yinyang.png" height="217px" width="217px" />

[Here](https://github.com/ashleyblackmore/terminator-solarized) is the github repo with the config file for Terminator.

## Vim ##

```bash
$sudo vim /etc/vim/vimrc
```

And uncomment 'syntax on'.

Next install [Pathogen](https://github.com/tpope/vim-pathogen):

```bash
##copy and paste this in the terminal:

mkdir -p ~/.vim/autoload ~/.vim/bundle; \
curl -Sso ~/.vim/autoload/pathogen.vim \
    https://raw.github.com/tpope/vim-pathogen/master/autoload/pathogen.vim
```

Now install [Solarized](http://ethanschoonover.com/) for Vim:

```bash
    $ cd ~/.vim/bundle
    $ git clone git://github.com/altercation/vim-colors-solarized.git
```

And install [NERDTree](https://github.com/scrooloose/nerdtree)

```bash
cd ~/.vim/bundle
git clone https://github.com/scrooloose/nerdtree.git
```

Now go ahead and creat a .vimrc:

```bash
$vim ~/.vimrc
```

Copy this in the .vimrc.
This will set a statusline and line numbers.
It activates pathogen and sets the colorscheme:


```bash vimrc
set statusline=%<%F%h%m%r%h%w%y\ %{&ff}\ %{strftime(\"%c\",getftime(expand(\"%:p\")))}%=\ lin:%l\,%L\ col:%c%V\ pos:%o\ ascii:%b\ %P
set laststatus=2
set number
execute pathogen#infect()
syntax enable
set background=light
colorscheme solarized
```

And the result will be this:

![vim](/images/vim.png "vim")

You can also choose the solarized-dark, but I think the light version is easier on the eye.
Now you can go coding, be mighty productive and make lots of money. Or not.:)
