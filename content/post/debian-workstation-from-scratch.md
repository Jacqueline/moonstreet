+++
date = "2014-01-09"
title = "XFCE4 on Debian Netinstall (Jessie) in VMware Workstation"
categories = ["linux","debian"]
tags = ["linux"]
+++

I have to admit that trying out Linux Distributions is one of my guilty pleasures. I enjoy configuring my desktop just the way I want it and to be in full control. Working in the terminal makes me very productive and now that I'm getting the hang of Vim it's totally addictive.
I've tried Vim (and NERDTree) on Windows 8.1 but it's just not the same. For instance, if you drop to a shell the dosbox opens whereas I prefer to stay in the same terminal.

I was very impressed by the simplicity of [Crunchbang](http://www.crunchbang.org) that I decided to try and create my own Linux distro from scratch. This is what I did.  

You can download Debian Jessie [here](http://cdimage.debian.org/cdimage/daily-builds/daily/arch-latest/i386/iso-cd/).

Install Jessie with the options that suit you. It becomes critical at the Software selection part. Make sure you select only the Standard System utilities:

![installer](/images/debian-installer.png "Installer")

Then finish the installation and logon as root.
These are the first apps to install.
Then type: visudo:


```bash
$apt-get install sudo vim curl build-essential linux-headers-$(uname -r) sudo

$visudo
```

I'm going to add my personal logon account to the sudoers, like this:

![visudo](/images/visudo.png "visudo")

If you hit 'ctrl-o' it will ask you to which file to save. Just accept what it suggests and press enter.

It's also wise to disable the annoying beep that you get to hear by each and every typo:

```
vim /etc/inputrc
```
Uncomment 'set bell-style none'

```
vim /etc/vim/vimrc
```
Add 'set vb'. Also enable syntax here by uncommenting: syntax on.

Now logoff and logon with your user account, because now you can use sudo.

###Enable free and contrib repo's and install vmware-tools

Open /etc/apt/sources.list and change it as follows.

```
$ sudo vim /etc/apt/sources.list
:%s/main/main contrib non-free

$sudo apt-get update && apt-get upgrade
```

Insert the Vmware tools CD and mount it as follows:

```
$ sudo mkdir /mnt/cdrom
$ sudo mount -t iso9660 -o ro /dev/cdrom /mnt/cdrom
```

Now the Vmware tools are in /mnt/cdrom and you can install them as usual.

Enable fancy dircolors and syntax coloring in vim

```
$ vim ~/.bashrc # uncomment dircolors

```

###Install XFCE

```
$ sudo apt-get install xorg xfce4 lightdm wicd gdebi
$ sudo apt-get install xfce4-goodies
$ sudo apt-get install git gitk
$ sudo apt-get install gtk2-engines-murrine
```

You can also combine this to one command.

Restart and login your new XFCE4 desktop.

###Eyecandy

Go to Settings --> Appearance and enable anti-aliasing under the font tab:

![appearance](/images/appearance.png "appaerance")

Enable transparent label text background on the desktop:

```
$ vim ~/.gtkrc-2.0
```

Add the following text:

```bash
style "xfdesktop-icon-view" {
XfdesktopIconView::label-alpha = 0
}
widget_class "*XfdesktopIconView*" style "xfdesktop-icon-view"

# You can also change the font colors.

style "xfdesktop-icon-view" {
XfdesktopIconView::label-alpha = 0

base[NORMAL] = "#ffffff"
base[SELECTED] = "#5D97D1"
base[ACTIVE] = "#5D97D1"

fg[NORMAL] = "#ffffff"
fg[SELECTED] = "#ffffff"
fg[ACTIVE] = "#ffffff"
}
widget_class "*XfdesktopIconView*" style "xfdesktop-icon-view"
```

Open Settings --> Window Manager Tweaks --> Compositor and enable display compositing:

![tweaks](/images/tweaks.png "tweak")

Install a theme. My favorite is [Albatross](http://shimmerproject.org/project/albatross/"). Just create a folder ~/.themes and clone the Albatross theme from [github](https://github.com/shimmerproject/Albatross.git)

###Result
And then you end up with something like this:

![desk](/images/xfce4-desk.png "desk")

And another Linux desktop is installed.
