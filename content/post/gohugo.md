+++
title = "Go Hugo!"
date = "2016-11-27T16:12:23+01:00"

+++


De dames van Moonstreet zijn al een paar jaar op zoek naar een prettig blogplatform.
We hebben de volgende wensen:

* Geen database
* Markdown support
* Goedkoop
* Gemakkelijk in gebruik

Met die eerste requirement vallen er al een hoop af (Wordpress bijvoorbeeld). De overige wensen schreeuwen om een statische platte website. We hebben gekeken naar Octopress en Jekyll maar die luisteren nogal nauw qua Rubygems. Ik zat dus niet in de dll hell, maar in de gem hell. [En dat is ook geen pretje](http://codebrane.com/blog/2010/11/25/welcome-to-ruby-gem-hell-or-how-not-to-spend-your-life-configuring-instead-of-coding/).

Toen viel mijn oog op https://gohugo.io. Die wordt het.

![moonstreet](https://gohugo.io/img/hugo-logo.png "Go Hugo")

En wat ook erg cool is, we doen aan continuous deployment met [Bitbucket & Aerobatic](https://gohugo.io/tutorials/hosting-on-bitbucket/). Dus we hoeven alleen onze code te committen naar Bitbucket en Aerobatic doet de rest.



Wat gaan we doen?

* Bloggen over van alles wat te maken heeft met tech, IT en security
* Code snippets
* Trucs en tips
* Video's!
