+++
date = "2013-11-24"
title = "Nodejs, Mongoose and Express"
categories = ["node","code", "javascript"]
tags = ["code"]
+++


Onto the next step on my journey learning Node.

I'm going to create registration system for informale care volunteers.
Volunteers can sign in and provide all sort of details about themselves. If you are a volunteer and you want to go on vacation or just need some rest, you can browse the directory and find a replacement volounteer.

We basically need an addressbook of volunteers.

The first functional specs are: </br>
* we need a _volunteer_</br>
* the volunteer can have a _status_ like available or unavailable</br>
* the volunteer has all sorts of charecteristics which he or she may edit herself.</br>
* the volunteer should be able to logon and may only chanqe her or his own details.


We also have some technical requirements</br>
* we want to write one language for the whole stack</br>
* the app should be platform indepent, lightweight and open source</br>


We can meet the tech reqs by using a webserver running on node.js.
For the database we choose MongoDb and Mongoose for the ORM, because these seem to be the most [popular](https://npmjs.org/browse/star).


## Designing the API ##

__/volunteers/1234:__ </br>
GET: show volunteer 1234 </br>
PUT: if exists updates volunteer 1234 </br>
DELETE: delete volunteer 1234 </br>
POST: will not work

__/volunteers:__ </br>
GET: read </br>
PUT: bulk update volunteers </br>
DELETE: deletes all volunteers </br>
POST: creates new volunteer </br>

## The first tests ##

```bash
mkdir iVolunteer
cd iVolunteer
npm install mocha
npm install should
npm install mongoose

mkdir test
vim volunteertest.js
```

I studied the Mongoose and Mocha sites thoroughly to get this. For me it is better to read the documentation from the code maintainer, because then I know I'm using the right syntax.


```javascript
var should = require('should');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var volunteerSchema = new Schema({
  firstname:  String,
  lastname: String
});


var Volunteer = mongoose.model('Volunteer', volunteerSchema);
mongoose.connect('mongodb://localhost:27017/betadb');

describe('Volunteer', function(){

  beforeEach(function(done){
    //clean the database:
    Volunteer.remove(done);
  });

  describe('#save()', function() {
    it('should save', function(done) {
      var volunteer = new Volunteer({firstname: 'Joe' })
        volunteer.save(function(err) {
          if (err) return done(err);  
          //assert.equal(volunteer.firstname,'Joe');
          volunteer.should.have.property('firstname','Joe');
          done();
        });
      });
   });
});
```
