
+++
date = "2013-11-21"
title = "Installing Node and Webstorm on Debian"
categories = ["node","code"]
tags = ["code"]
+++

[This](https://gist.github.com/isaacs/579814) is how to install Node on a Linux system:

```bash
#Install Node
echo 'export PATH=$HOME/local/bin:$PATH' >> ~/.bashrc
. ~/.bashrc
mkdir ~/local
mkdir ~/node-latest-install
cd ~/node-latest-install
curl http://nodejs.org/dist/node-latest.tar.gz | tar xz --strip-components=1
./configure --prefix=~/local
make install # ok, fine, this step probably takes more than 30 seconds...
curl https://npmjs.org/install.sh | sh
```

For Webstorm we need the SunJDK.


```bash
#Install SunJDK
#get the JDK
$ wget --no-cookies --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com" "http://download.oracle.com/otn-pub/java/jdk/7u45-b18/jdk-7u45-linux-i586.tar.gz"

#unpack it
$ sudo tar zxvf jdk-7u45-linux-i586.tar.gz -C /usr/lib/jvm/

```

Check the current JVM (openJDK):

```bash
update-alternatives --display java
java - auto mode
  link currently points to /usr/lib/jvm/java-6-openjdk-i386/jre/bin/java
/usr/lib/jvm/java-6-openjdk-i386/jre/bin/java - priority 1061
  slave java.1.gz: /usr/lib/jvm/java-6-openjdk-i386/jre/man/man1/java.1.gz
Current 'best' version is '/usr/lib/jvm/java-6-openjdk-i386/jre/bin/java'.
```

Check the -priority value. Mine is **1061** as you can see..
This means we need to install the SunJDK with a higher priority, i.c. **1062**.

Run all these commands (make sure the path is right though).

```bash
# set the priority to 1062!
$ sudo update-alternatives --install /usr/bin/java java /usr/lib/jvm/jdk1.7.0_45/bin/java 1062
$ sudo update-alternatives --install /usr/bin/javac javac /usr/lib/jvm/jdk1.7.0_45/bin/javac 1062
$ sudo update-alternatives --install /usr/bin/javaws javaws /usr/lib/jvm/jdk1.7.0_45/bin/javaws 1062
$ sudo update-alternatives --install /usr/lib/mozilla/plugins/libnpjp2.so libnpjp2.so /usr/lib/jvm/jdk1.7.0_45/jre/lib/i386/libnpjp2.so 1062

#check the java version
/usr/lib/jvm$ java -version
java version "1.7.0_45"
Java(TM) SE Runtime Environment (build 1.7.0_45-b18)
Java HotSpot(TM) Client VM (build 24.45-b08, mixed mode)
```

Install WebStorm

```bash
wget http://download.jetbrains.com/webstorm/WebStorm-7.0.2.tar.gz
```
