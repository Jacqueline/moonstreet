+++
date = "2013-11-22"
title = "a Node HTTP Server"
categories = ["node","code","js"]
tags = ["code"]
+++


Just like c# (which is my first language), you can import libraries with Node. They're called modules.
One of these modules is 'http'. Let's see how we can create a http server with this module.

```javascript
//hellopizza.js
var http = require('http');

//the http module has a function: createServer which takes a callback as a parameter:

http.createServer(function(request,response) {
       response.writeHead('200');
       response.write("one pepperoni pizza please");
       response.end();

    }).listen(8080);

console.log("listening on 8080");

```

We can run this with node:

```bash
jacqueline:public$ node hellopizza.js
listening on 8080
```
Browse to the page with curl:

```bash
jacqueline:$ curl localhost:8080
one pepperoni pizza please
```
