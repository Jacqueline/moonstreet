+++
date = "2014-01-12"
title = "Debian on the bare metal"
categories = ["linux"]
tags = ["linux","debian"]
+++


Today I took the plunge and installed Debian instead of Windows 8.1 on my main machine. Indeed, on the bare metal.

I first checked if my hardware compatibility. There's an awesome (portable) tool called [HWiNFO]("http://www.hwinfo.com/") you can run and check what hardware is inside your computer. Then you can check if there's Linux support. The most important hardware pieces are video cards, networking hardware and audio.

For instance, I happen to own an Intel 4000 HD Graphics adapter. I had to compile and install the drivers.
Lucky for me, the README files were quite good.

Tips for optimizing graphics:

* Install xrandr to enable extended mode on your dual monitor setup

* Install gdm3 (however I'm not sure if this did the trick that my display suddenly became beautiful.


![desktop](/images/silverslank.png "desktop")

For Evernote and Spotify are no Linux packages, so I installed them as addins in Chrome. It makes them look like they are native. Suddenly I came to understand the importance of Chromebooks!

Next week I get to order a MacBook Pro (courtesy to my boss). Then Windows has disappeared form all my computers, except the virtual ones I need for work (and Visual Studio).
